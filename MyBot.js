const hlt = require('./hlt');
const { Direction } = require('./hlt/positionals');
const logging = require('./hlt/logging');

const game = new hlt.Game();
game.initialize().then(init);

// Logic here has 30 seconds to run before the game starts
async function init() {
	// TODO: Init logic
	logging.info(game.gameMap.naiveNavigate.toString());
	// Start game
	await game.ready('MyJavaScriptBot');

	logging.info(`My Player ID is ${game.myId}.`);

	gameLoop();
}

var movedShips = [];
var plannedMoves = [];

// Each "turn" (loop) has 2 seconds to run
async function gameLoop() {
	while (game.turnNumber <= hlt.constants.MAX_TURNS) {
		await game.updateFrame();

		movedShips = [];
		plannedMoves = [];

		let { gameMap, me } = game;

		let commandQueue = [];

		for (let ship of me.getShips()) {
			planMove(ship, ship.position);
		}

		// Loop through ships
		for (let ship of me.getShips()) {
			logging.info(JSON.stringify(ship));
			ship.isMoving = false;
			commandQueue.push(getMove(ship, me, gameMap));
			movedShips.push(ship.id);
		}

		// If game is not almost done && if we can afford a ship && if the shipyard is not occupied
		if (shouldBuyShip() &&
			!gameMap.get(me.shipyard).isOccupied &&
			!isPlanned(me.shipyard.position)) {
			logging.info('Spawning ship at ' + JSON.stringify(me.shipyard.position) + ' because it is not in\n' + JSON.stringify(plannedMoves));
			// Make a ship
			commandQueue.push(me.shipyard.spawn());
		}

		await game.endTurn(commandQueue);
	}
}

function shouldBuyShip() {
	if (game.turnNumber < 0.3 * hlt.constants.MAX_TURNS) return game.me.haliteAmount >= hlt.constants.SHIP_COST;
	return (game.turnNumber < 0.55 * hlt.constants.MAX_TURNS && game.me.haliteAmount >= hlt.constants.SHIP_COST * (game.me.shipCount * 2));
}

function planMove(ship, pos) {
	plannedMoves[ship.id] = pos ? Object.values(pos) : pos;
}
function isPlanned(move) {
	let pos = Object.values(move);
	for(let planned of plannedMoves) {
		if (!planned) continue;
		if (arrayEquals(pos, planned)) return true;
	}

	return false;
}

function moveShip(ship, mv) {
	ship.isMoving = true;
	logging.info('Planning move to ' + JSON.stringify(ship.position.directionalOffset(mv)) + ' as it is not contained in\n' + JSON.stringify(plannedMoves));
	planMove(ship, ship.position.directionalOffset(mv));
	return ship.move(mv);
}

function isSuicideTime(ship) {
	let pos = ship.position;
	let dest = game.me.shipyard.position;
	let distance = game.gameMap.calculateDistance(pos, dest);
	return game.turnNumber > hlt.constants.MAX_TURNS - (distance + 25);
}

function getOptimalMove(ship) {
	logging.info('Looking for best move for ' + JSON.stringify(ship.position));

	let bestPos = ship.position;
	let zeroCount = 0;
	let blockedCount = 0;
	let checked = 1;

	if (game.gameMap.get(bestPos).haliteAmount === 0) zeroCount++;

	for(let dir of Direction.getAllCardinals()) {
		let target = ship.position.directionalOffset(dir);
		let tile = game.gameMap.get(target);
		if (isPlanned(target)) {
			blockedCount++;
			continue;
		}
		checked++;
		if(tile.haliteAmount > game.gameMap.get(bestPos).haliteAmount) {
			logging.info(JSON.stringify(target) + ' has ' + tile.haliteAmount + ' halite, previous best is ' + game.gameMap.get(bestPos).haliteAmount);
			bestPos = target;
		} else {
			logging.info(JSON.stringify(target) + ' has ' + tile.haliteAmount + ' halite, but best is ' + game.gameMap.get(bestPos).haliteAmount);
		}
		if(tile.haliteAmount === 0) zeroCount++;
	}

	if (blockedCount === 4) {
		// Completely surrounded, flag self for suicide
		// TODO: Improve this
		planMove(ship, null);
	}
	if(zeroCount < checked) {
		logging.info('Best option was ' + JSON.stringify(bestPos) + ' (ZeroCount was ' + zeroCount + ' and checked was ' + checked + ')');
		return bestPos;
	} else {
		logging.info('All options were empty, moving at random');
		return ship.position.directionalOffset(Direction.getAllCardinals()[Math.floor(4 * Math.random())]);
	}
}

function getMove(ship, me, gameMap) {
	if (ship.haliteAmount < gameMap.get(ship.position).haliteAmount * 0.1 &&
		gameMap.get(ship.position).haliteAmount !== 0) {
		return ship.stayStill();
	}
	// If ship is more than half full
	if (ship.haliteAmount > hlt.constants.MAX_HALITE / 1.25 ||
		isSuicideTime(ship)) {
		// Start heading home
		let destination = me.shipyard.position;
		let safeMove = navigate(ship, destination, gameMap);
		return moveShip(ship, safeMove);
	}
	// If current tile has less than 100 halite remaining
	else if (gameMap.get(ship.position).haliteAmount < hlt.constants.MAX_HALITE / 10) {
		// Move in a random valid direction
		//let direction = Direction.getAllCardinals()[Math.floor(4 * Math.random())];
		//let destination = ship.position.directionalOffset(direction);
		let destination = getOptimalMove(ship);
		let safeMove = navigate(ship, destination, gameMap);
		return moveShip(ship, safeMove);
	}
	else {
		planMove(ship, ship.position);
		logging.info('Planning to stay at ' + JSON.stringify(ship.position));
		return ship.stayStill();
	}
}

function navigate(ship, destination, gameMap) {
	for(let direction of gameMap.getUnsafeMoves(ship.position, destination)) {
		let targetPos = ship.position.directionalOffset(direction);

		if (!isPlanned(targetPos) ||
			(isSuicideTime(ship) && targetPos.equals(game.me.shipyard.position))) {
			return direction;
		}
	}

	return Direction.Still;
}

function arrayEquals(a, b) {
	// Quick checks to save time
	if (a === null || a === undefined || b === null || b === undefined) return false;
	if (a.length === 0 && b.length === 0) return true;
	if (a.length !== b.length) return false;
	if (a === b) return true;

	// Clone arrays to safely sort
	let a2 = a.slice();
	let b2 = b.slice();

	// Sort to disregard value order
	a2.sort();
	b2.sort();

	let equal = true;
	for(let index in a2) {
		if (a2[index] !== b2[index]) {
			equal = false;
			break;
		}
	}

	return equal;
}
