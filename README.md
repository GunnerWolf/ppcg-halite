# PPCG Halite
This is the repo for the PPCG team's entry to [Halite III](https://halite.io)

## Contributing

We chose JavaScript as our language, as it's one of the most common languages used in PPCG.

You can chat with us in the [StackExchange chatroom](https://chat.stackexchange.com/rooms/86456/ppcg-halite) about what
work needs to be done. Note that this requires you have at least 20 reputation on StackExchange.

We use ESLint to maintain code quality. It is set up as a dev dependency in NPM, and any contributions should not flag
any warnings or errors in eslint.

There are several `TODO` comments in the code, these indicate known issues/improvements to be made. Feel free to start
work on any of these, however please first check in the above linked chatroom that nobody else is working on it.

## Running the bot

Running the `run_game.sh` shell script (or the `run_game.bat` batch script on windows) will run a test Halite game
locally, with the bot playing against itself. This will output important information such as warnings, errors, and the
winner, and will also generate log files for the bot.  
Additionally, this will create a replay file (`.hlt`) which can be uploaded to the halite site to watch the game
visually.

## Submitting the bot

As far as I know (the official rules contradict what Halite admins say on the forums) when any team member submits a
bot, it counts as the team's bot, rather than the users. This means that any team member can submit a new version of the
bot (as the team can only have 1 bot). However, to avoid confusion, please only submit the latest working version of
this repo, and please announce in the above linked chatroom when you are updating the bot.
